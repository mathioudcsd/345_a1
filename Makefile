all: shell

shell:  shell.o tools3.o
	gcc tools3.o shell.o -o shell | grep -v "note:"

shell.o: shell.c
	gcc -c shell.c | grep -v "note:"

tools3.o: tools3.c
	gcc -c tools3.c | grep -v "note:"

clean:
	rm shell tools3.o shell.o
